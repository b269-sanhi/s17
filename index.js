/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:


function details(){
	let fullName = prompt('What is your name?');
	let age = prompt('How old are you?');
	let address = prompt('Where do you live?');
console.log('Hello, ' + fullName);
console.log('You are  ' + age + '' + 'years old');
console.log('You live in ' + address);
}
details();
/*


printWelcomeMessage();
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:


function music (){
	let fav1 = ['Coldplay']
	console.log('1.'+ ' ' + fav1)
	let fav2 = ['James Arthur']
	console.log('2.'+ ' ' + fav2)
	let fav3 = ['Rivermaya']
	console.log('3.'+ ' ' + fav3)
	let fav4 = ['Parokya ni Edgar']
	console.log('4.'+ ' ' + fav4)
	let fav5 = ['Eraserheads']
	console.log('5.'+ ' ' + fav5)

}
music();
/*

	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function favorite (){
	let fav1 = ['Titanic']
	console.log('1.'+ ' ' + fav1)
	console.log('Rotten Tomatoes Rating is: 88%')
	let fav2 = ['50 first dates']
	console.log('2.'+ ' ' + fav2)
	console.log('Rotten Tomatoes Rating is: 45%')
	let fav3 = ['The green mile']
	console.log('3.'+ ' ' + fav3)
	console.log('Rotten Tomatoes Rating is: 79%')
	let fav4 = ['Good will hunting']
	console.log('4.'+ ' ' + fav4)
	console.log('Rotten Tomatoes Rating is: 97%')
	let fav5 = ['Dead Poets Society']
	console.log('5.'+ ' ' + fav5)
	console.log('Rotten Tomatoes Rating is: 84%')
}
favorite();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

alert("Hi! Please add the names of your friends.");
function printFriends() {
	 	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friends); 
	console.log(printFriends); 
};


